Ateliers de fabrication d'un quadcopter autonome, à la Maison de Quartier La Touche. 

== Idées de départ ==
* Cela fait des années que nous réalisons des petits projets de robots, en grande majorité à l'aide de cartes arduino.
* Les ados de la Maison de Quartier La Touche demandent depuis longtemps qu'on se lance dans la fabrication d'un drone.
* La réglementation parait lourde dans le cadre d'un vol en extérieur, en particulier sur l'espace public. Nous décidons donc de penser le projet pour un vol en intérieur (salle des fêtes, gymnase, etc...)
* Nous ne souhaitons pas fabriquer un drone pilotable avec une télécommande : nous voulons relever le défi de réaliser un drone capable de se déplacer tout seul, en évitant les obstacles, en faisant demi tour s'il arrive contre un mur, etc...

== Liens externes ==
https://diydrones.com/profiles/blogs/arduimu-quadcopter-part-iii : un exemple de drone avec des capteurs de distance. La vidéo dans le parking montre bien un exemple de ce qu'on veut réaliser

http://www.electronoobs.com/eng_robotica_tut6_2.php : on va pas y couper, il va falloir se plonger dans les boucles PID, pour corriger les consignes (inclinaison, hauteur) avec les mesures effectives.

== Matériel ==
1. Un chassis

2. 4 moteurs, 4 ESC et 4 hélices

3. Une batterie

4. Une carte arduino micro

5. 4 capteurs de distance infrarouge

6. <s>un gyroscope</s> deux sonars HC-SR04. Après tests, le gyroscope n'est pas assez précis pour un vol en intérieur

7. un baromètre

8. un capteur bluetooth

Pour plus d'explications :

1. Le Realacc 220 est un chassis de 22cm (22cm de diagonale, c'est la distance entre les 2 centres d'hélices). Nous avons cherché un compromis entre un chassis assez gros pour embarquer tous nos capteurs électroniques, tout en essayant de ne pas être trop volumineux, étant donné qu'il est destiné à voler en intérieur. 

2. Nous avons acheté un kit complet, connu pour être adapté à un chassis de 22cm. Si l'on veut aller dans le détail, il faut choisir les moteurs en fonction du poids de notre drone et de la vitesse maximale que l'on souhaite. Puis choisir des ESC avec une puissance suffisante pour les moteurs. Puis choisir les hélices adaptées. Notre kit rassemble des moteurs Racerstar 2205, des ESC 30A et des hélices 5 pouces. On verra bien ce que ça donne.

3. La batterie Li-po que nous avons choisie est une 3S 70C 1500mAh. 

4. La carte de vol sera une carte arduino, que nous allons programmer. Nous pensons partir sur une carte arduino micro pour qu'elle soit la plus légère possible. 

5. Les 4 capteurs infrarouges seront des Sharp GP2Y0A21YK0F, qui sont capables de détecter les obstacles entre 10 et 80 cm.

6. <s>Le gyroscope est un MPU6050.</s> Un sonar orienté vers le haut pour ne pas se cogner au plafond, un autre orienté vers le bas pour gérer la hauteur du drone. 

7. Le baromètre est un BMP180

8. Le capteur bluetooth est un HC05

