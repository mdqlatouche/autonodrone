/**
 * Permet de recevoir un signal bluetooth et de l'envoyer vers les moteurs
 * L'appli mobile qui envoie le signal a été développée sur MIT AppInventor
 */
#include <Servo.h> // on appelle la bibilothèque Servo pour envoyer la consigne de vitesse à l'ESC
#include <SoftwareSerial.h> // bibliothèque pour avoir plusieurs ports séries
SoftwareSerial HC05(8,13);//TX,RX sur HC06 (serait RX,TX sur arduino)
Servo motA, motB, motC, motD;   // Création de l'objet permettant le contrôle de l'ESC
 
void setup() {
    motA.attach(9, 1000, 2000); // 1000 and 2000 are very important ! Values can be different with other ESCs.
    motB.attach(10, 1000, 2000);
    motC.attach(11, 1000, 2000);
    motD.attach(7, 1000, 2000); delay(15);
   Serial.begin(9600); // on ouvre la connexion avec le port Série
   motA.write(0);
  motB.write(0);
  motC.write(0);
  motD.write(0);
  
   HC05.begin(9600); // on ouvre la connexion bluetooth
  
  }
 
void loop() {

    while(HC05.available()) // quand on reçoit quelque chose depuis le bluetooth
        {
          delay(3);
          int c = HC05.read(); // si on veut lire du texte, il faut remplacer int par char et mettre ci-dessous "texte += c"
          //c=c/4; // rajouté le 10/05/2019 : le potar envoyait trop de patate, on divise donc la puissance par 4
          Serial.println(c); // montre la valeur dans le moniteur Série
          
      motA.write(c);
      motB.write(c);
      motC.write(c);
      motD.write(c);  
        }
     
}
