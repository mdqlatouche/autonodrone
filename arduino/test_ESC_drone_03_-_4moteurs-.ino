/**
 * Permet d'envoyer via le moniteur série la vitesse vers les ESC
 */
#include <Servo.h>
 
Servo motA, motB, motC, motD;   // Création de l'objet permettant le contrôle de l'ESC
 
int val = 0; // 
 
void setup() {
   motA.attach(9, 1000, 2000); // 1000 and 2000 are very important ! Values can be different with other ESCs.
    motB.attach(10, 1000, 2000);
    motC.attach(11, 1000, 2000);
    motD.attach(7, 1000, 2000);delay(15);
   Serial.begin(9600);
  motA.write(0);
  motB.write(0);
  motC.write(0);
  motD.write(0);
   
   // Quelques informations pour l'utilisateur
   Serial.println("Saisir un nombre entre 0 et 179");
   Serial.println("(0 = arret - 179 = vitesse maxi");
   Serial.println(" demarrage a partir de 20)");
   }
 
void loop() {
   if (Serial.available() > 0) {
      val = Serial.parseInt();   // lecture de la valeur passée par le port série
      Serial.println(val);
      motA.write(val);            // 
      motB.write(val);            // 
      motC.write(val);            // 
      motD.write(val);            // 
      delay(15);
      }
   }
