/**
 * Permet d'envoyer via le moniteur série la vitesse vers un ESC
 */

#include <Servo.h>
 
Servo esc;   // Création de l'objet permettant le contrôle de l'ESC
 
int val = 0; // 
 
void setup() {
   esc.attach(12, 1000, 2000); // On attache l'ESC au port numérique  (port PWM obligatoire)
   delay(15);
   Serial.begin(9600);
  esc.write(0);
   
   // Quelques informations pour l'utilisateur
   Serial.println("Saisir un nombre entre 0 et 179");
   Serial.println("(0 = arret - 179 = vitesse maxi");
   Serial.println(" demarrage a partir de 20)");
   }
 
void loop() {
   if (Serial.available() > 0) {
      val = Serial.parseInt();   // lecture de la valeur passée par le port série
      Serial.println(val);
      esc.write(val);            // 
      delay(15);
      }
   }
